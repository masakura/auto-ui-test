import org.junit.jupiter.api.Test
import org.openqa.selenium.chrome.ChromeDriver
import kotlin.test.assertTrue

class SeleniumTest {
    @Test
    fun testSample() {
        val driver = WebDriverFactory.create()

        try {
            driver.get("https://www.selenium.dev/")

            assertTrue(driver.title.contains("SeleniumHQ"))
        }
        finally {
            driver.quit()
        }
    }

    private fun create() = ChromeDriver()
}
