import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import java.net.URL

object WebDriverFactory {
     fun create(): RemoteWebDriver {
          return grid() ?: chromeDriver()
     }

     private fun chromeDriver() = ChromeDriver()

     private fun grid(): RemoteWebDriver? {
          val remoteAddress = System.getenv("SELENIUM_HUB_ADDRESS")

          if (remoteAddress == null || remoteAddress.isEmpty()) return null

          return RemoteWebDriver(URL(remoteAddress), DesiredCapabilities.chrome())
     }
}